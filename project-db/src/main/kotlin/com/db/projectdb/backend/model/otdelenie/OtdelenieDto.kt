package com.db.projectdb.backend.model.otdelenie

import com.db.projectdb.backend.model.BaseDto

data class OtdelenieDto(
    val nazvOtdel: String,
    val nomerPodrazdelenia: String,
    val nazvanieArmii: String,
    val tipINomerObedinenia: String,
    val nazvRoti: String,
    val nazvVzvoda: String,
    val mesto: String
) : BaseDto