package com.db.projectdb.backend.model.voorugenie

import com.db.projectdb.backend.model.BaseDto

data class VoorugenieDto(
    val tipVoorug: String,
    val kolVoVoorug: String,
    val nomerPodrazdelenia: String
) : BaseDto