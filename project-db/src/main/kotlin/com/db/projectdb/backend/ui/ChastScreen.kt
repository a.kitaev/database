package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.chast.Chast
import com.db.projectdb.backend.model.chast.ChastDto
import com.db.projectdb.backend.model.chast.ChastKey
import com.db.projectdb.backend.model.select_dto.ChastAndKomandir
import com.db.projectdb.backend.service.ChastService
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.router.Route

@Route("chast")
open class ChastScreen(
    service: ChastService
) : BaseTableScreen<Chast, ChastDto>(service) {

    private val gridForSelect = Grid<ChastAndKomandir>()
    private val gridForSelect2 = Grid<ChastDto>()

    init {
        grid.addColumn(ChastDto::name)
            .setHeader("Название части")
        grid.addColumn(ChastDto::mesto)
            .setHeader("Место")
        grid.addColumn(ChastDto::nazvanieArmii)
            .setHeader("Название армии")
        grid.addColumn(ChastDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        grid.addColumn(ChastDto::tipINomerObedinenia)
            .setHeader("Тип и номер объединения")
        this.add(grid)

        // text field for select
        val textField = TextField()
        this.add(textField)

        val selectArea = HorizontalLayout().apply {
            add(ButtonUtil.generateButtonWithField("Части по армии", textField) {
                val dataProvider: DataProvider<ChastAndKomandir, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByArmia(textField.value)
                }, {
                    service.getStreamDtoByArmia(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })

            add(ButtonUtil.generateButtonWithField("Части по объединению", textField) {
                val dataProvider: DataProvider<ChastAndKomandir, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByObedinenie(textField.value)
                }, {
                    service.getStreamDtoByObedinenie(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })
        }

        gridForSelect.addColumn(ChastAndKomandir::fio)
            .setHeader("ФИО")
        gridForSelect.addColumn(ChastAndKomandir::kodSlugashego)
            .setHeader("Код коммандира")
        gridForSelect.addColumn(ChastAndKomandir::mesto)
            .setHeader("Место дислокации части")
        gridForSelect.addColumn(ChastAndKomandir::nazvanieChasti)
            .setHeader("Название части")
        gridForSelect.addColumn(ChastAndKomandir::nazvanieArmii)
            .setHeader("Название армии")
        gridForSelect.addColumn(ChastAndKomandir::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        gridForSelect.addColumn(ChastAndKomandir::tipINomerObedinenia)
            .setHeader("Тип и номер подразделения")
        gridForSelect.addColumn(ChastAndKomandir::zvanie)
            .setHeader("Звание коммандира")
        this.add(selectArea)
        this.add(gridForSelect)

        // text field for second select
        val textField2 = TextField()
        this.add(textField2)

        val selectArea2 = HorizontalLayout().apply {
            add(ButtonUtil.generateButtonWithField("Части по месту дислокации", textField2) {
                val dataProvider: DataProvider<ChastDto, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByPlace(textField2.value)
                }, {
                    service.getStreamDtoByPlace(textField2.value).count().toInt()
                })
                gridForSelect2.dataProvider = dataProvider
            })
        }

        gridForSelect2.addColumn(ChastDto::name)
            .setHeader("Название части")
        gridForSelect2.addColumn(ChastDto::mesto)
            .setHeader("Место")
        gridForSelect2.addColumn(ChastDto::nazvanieArmii)
            .setHeader("Название армии")
        gridForSelect2.addColumn(ChastDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        gridForSelect2.addColumn(ChastDto::tipINomerObedinenia)
            .setHeader("Тип и номер объединения")
        this.add(selectArea2)
        this.add(gridForSelect2)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Название части"),
            TextField("Место"),
            TextField("Название армии"),
            TextField("Номер подразделения"),
            TextField("Тип и номер объединения")
        )
    }

    override fun buildEntity(args: List<String>): Chast {
        return Chast(
            id = ChastKey(
                mesto = args[1],
                nazvanieArmii = args[2],
                nomerPodrazdelenia = args[3],
                tipINomerObedinenia = args[4]
            ),
            name = args[0]
        )
    }
}