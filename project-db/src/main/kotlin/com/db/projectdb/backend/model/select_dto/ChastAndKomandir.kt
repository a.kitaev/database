package com.db.projectdb.backend.model.select_dto

data class ChastAndKomandir(
    val nazvanieChasti: String,
    val nomerPodrazdelenia: String,
    val mesto: String,
    val tipINomerObedinenia: String,
    val nazvanieArmii: String,
    val fio: String,
    val zvanie: String,
    val kodSlugashego: String
)

