package com.db.projectdb.backend.model.voorugenie

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class VoorugenieKey(
    @Column(name = "Tip_voorug")
    val tipVoorug: String,
    @Column(name = "Kol_vo_voorug")
    val kolVoVoorug: String,
    @Column(name = "Nomer_podrazdelenia")
    val nomerPodrazdelenia: String
) : Serializable