package com.db.projectdb.backend.model.sostav

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.Column
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "sostav")
data class Sostav(
    @EmbeddedId
    val id: SostavKey,

    @Column(name = "Special")
    val special: String,
    @Column(name = "Tip_sostava")
    val tipSostava: String,
    @Column(name = "Podchinaetca")
    val podchinaetca: String
) : BaseEntity