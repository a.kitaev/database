package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.select_dto.ChastAndVoorugenie
import com.db.projectdb.backend.model.voorugenie.Voorugenie
import com.db.projectdb.backend.model.voorugenie.VoorugenieDto
import com.db.projectdb.backend.model.voorugenie.VoorugenieKey
import com.db.projectdb.backend.service.VoorugenieService
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.router.Route

@Route("voorugenie")
open class VoorugenieScreen(
    service: VoorugenieService
) : BaseTableScreen<Voorugenie, VoorugenieDto>(service) {

    val gridForSelect = Grid<ChastAndVoorugenie>()

    init {
        grid.addColumn(VoorugenieDto::tipVoorug)
            .setHeader("Тип вооружения")
        grid.addColumn(VoorugenieDto::kolVoVoorug)
            .setHeader("Количество единиц вооружения")
        grid.addColumn(VoorugenieDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        this.add(grid)

        // text field for select
        val textField = TextField()
        this.add(textField)

        val selectArea = HorizontalLayout().apply {
            add(ButtonUtil.generateButtonWithField("Вооружение по часте", textField) {
                val dataProvider: DataProvider<ChastAndVoorugenie, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByChast(textField.value)
                }, {
                    service.getStreamDtoByChast(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })

            add(ButtonUtil.generateButtonWithField("Вооружение по армии", textField) {
                val dataProvider: DataProvider<ChastAndVoorugenie, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByArmy(textField.value)
                }, {
                    service.getStreamDtoByArmy(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })

            add(ButtonUtil.generateButtonWithField("Части по типу вооружения с >10", textField) {
                val dataProvider: DataProvider<ChastAndVoorugenie, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByVoorugenieType(textField.value)
                }, {
                    service.getStreamDtoByVoorugenieType(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })
        }

        gridForSelect.addColumn(ChastAndVoorugenie::tipVoorug)
            .setHeader("Тип вооружения")
        gridForSelect.addColumn(ChastAndVoorugenie::kolVoVoorug)
            .setHeader("Количество оружия")
        gridForSelect.addColumn(ChastAndVoorugenie::nazvanieChasti)
            .setHeader("Название части")
        gridForSelect.addColumn(ChastAndVoorugenie::nazvanieArmii)
            .setHeader("Название армии")
        gridForSelect.addColumn(ChastAndVoorugenie::mesto)
            .setHeader("Место дислокации")
        gridForSelect.addColumn(ChastAndVoorugenie::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        gridForSelect.addColumn(ChastAndVoorugenie::tipINomerObedinenia)
            .setHeader("Тип и номер объединения")
        this.add(selectArea)
        this.add(gridForSelect)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Тип вооружения"),
            TextField("Количество единиц вооружения"),
            TextField("Номер подразделения")
        )
    }

    override fun buildEntity(args: List<String>): Voorugenie {
        return Voorugenie(
            id = VoorugenieKey(
                tipVoorug = args[0],
                kolVoVoorug = args[1],
                nomerPodrazdelenia = args[2]
            )
        )
    }
}