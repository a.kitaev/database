package com.db.projectdb.backend.model.vzvod

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "vzvod")
data class Vzvod(
    @EmbeddedId
    val id: VzvodKey
) : BaseEntity