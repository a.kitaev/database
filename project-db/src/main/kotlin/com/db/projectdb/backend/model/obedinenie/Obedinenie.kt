package com.db.projectdb.backend.model.obedinenie

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "obedinenie")
data class Obedinenie(
    @EmbeddedId
    var id: ObedinenieKey
) : BaseEntity