package com.db.projectdb.backend.model.select_dto

data class ChastAndVoorugenie(
    val nazvanieChasti: String,
    val nomerPodrazdelenia: String,
    val mesto: String,
    val tipINomerObedinenia: String,
    val nazvanieArmii: String,
    val tipVoorug: String,
    val kolVoVoorug: String
)

