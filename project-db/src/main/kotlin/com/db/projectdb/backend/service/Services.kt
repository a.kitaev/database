package com.db.projectdb.backend.service

import com.db.projectdb.backend.model.armia.Armia
import com.db.projectdb.backend.model.armia.ArmiaDto
import com.db.projectdb.backend.model.chast.Chast
import com.db.projectdb.backend.model.chast.ChastDto
import com.db.projectdb.backend.model.obedinenie.Obedinenie
import com.db.projectdb.backend.model.obedinenie.ObedinenieDto
import com.db.projectdb.backend.model.otdelenie.Otdelenie
import com.db.projectdb.backend.model.otdelenie.OtdelenieDto
import com.db.projectdb.backend.model.podrazdelenie.Podrazdelenie
import com.db.projectdb.backend.model.podrazdelenie.PodrazdelenieDto
import com.db.projectdb.backend.model.rota.Rota
import com.db.projectdb.backend.model.rota.RotaDto
import com.db.projectdb.backend.model.select_dto.*
import com.db.projectdb.backend.model.soorugenia.Soorugenia
import com.db.projectdb.backend.model.soorugenia.SoorugeniaDto
import com.db.projectdb.backend.model.sostav.Sostav
import com.db.projectdb.backend.model.sostav.SostavDto
import com.db.projectdb.backend.model.tehnika.Tehnika
import com.db.projectdb.backend.model.tehnika.TehnikaDto
import com.db.projectdb.backend.model.voorugenie.Voorugenie
import com.db.projectdb.backend.model.voorugenie.VoorugenieDto
import com.db.projectdb.backend.model.vzvod.Vzvod
import com.db.projectdb.backend.model.vzvod.VzvodDto
import com.db.projectdb.backend.repository.*
import org.springframework.stereotype.Service
import java.util.stream.Stream

@Service
data class ArmiaService(
    val rep: ArmiaRepository,
    val repChast: ChastRepository
) : BaseService<Armia, ArmiaDto>(rep) {
    override fun getStreamDto() = findAll().map {
        ArmiaDto(it.id.nomerPodrazdelenia, it.id.nazvanieArmii)
    }.stream()

    fun getStreamDtoMaxChast(): Stream<ArmyChastCount> {
        return try {
            val max = getStreamDtoChast()
                .max { o1, o2 -> o1.kolvoChastey.toInt().compareTo(o2.kolvoChastey.toInt()) }
            max?.get() ?: return arrayListOf<ArmyChastCount>().stream()
            return arrayListOf(max.get()).stream()
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ArmyChastCount>().stream()
        }
    }

    fun getStreamDtoMinChast(): Stream<ArmyChastCount> {
        return try {
            val max = getStreamDtoChast()
                .min { o1, o2 -> o1.kolvoChastey.toInt().compareTo(o2.kolvoChastey.toInt()) }
            max?.get() ?: return arrayListOf<ArmyChastCount>().stream()
            return arrayListOf(max.get()).stream()
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ArmyChastCount>().stream()
        }
    }

    private fun getStreamDtoChast(): Stream<ArmyChastCount> {
        return try {
            val map = mutableMapOf<String, Int>()
            repChast.findAll()
                .stream()
                .forEach {
                    if (!map.containsKey(it.id.nazvanieArmii.trim())) {
                        map[it.id.nazvanieArmii.trim()] = 0
                    }
                    map[it.id.nazvanieArmii.trim()] = map[it.id.nazvanieArmii.trim()]!! + 1
                }
            return map.map { ArmyChastCount(it.key, it.value.toString()) }.stream()
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ArmyChastCount>().stream()
        }
    }
}

@Service
data class ChastService(
    val rep: ChastRepository,
    val repSostav: SostavRepository
) : BaseService<Chast, ChastDto>(rep) {
    override fun getStreamDto() = findAll().map {
        ChastDto(it.id.nomerPodrazdelenia, it.id.tipINomerObedinenia, it.id.nazvanieArmii, it.id.mesto, it.name)
    }.stream()

    fun getStreamDtoByObedinenie(tipINomerObedinenia: String): Stream<ChastAndKomandir> {
        return try {
            rep.findAllById_TipINomerObedineniaContains(tipINomerObedinenia.trim())
                .stream()
                .filter { repSostav.findAllById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia.trim()).isNotEmpty() }
                .map {
                    val komandir = repSostav.findFirstById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia)
                    ChastAndKomandir(it.name, it.id.nomerPodrazdelenia, it.id.mesto, it.id.tipINomerObedinenia, it.id.nazvanieArmii, komandir.id.fio, komandir.id.zvanie, komandir.id.kodSlugashego)
                }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ChastAndKomandir>().stream()
        }
    }

    fun getStreamDtoByArmia(nazvanieArmii: String): Stream<ChastAndKomandir> {
        return try {
            rep.findAllById_NazvanieArmiiContains(nazvanieArmii.trim())
                .stream()
                .filter { repSostav.findAllById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia).isNotEmpty() }
                .map {
                    val komandir = repSostav.findFirstById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia)
                    ChastAndKomandir(it.name, it.id.nomerPodrazdelenia, it.id.mesto, it.id.tipINomerObedinenia, it.id.nazvanieArmii, komandir.id.fio, komandir.id.zvanie, komandir.id.kodSlugashego)
                }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ChastAndKomandir>().stream()
        }
    }

    fun getStreamDtoByPlace(mesto: String): Stream<ChastDto> {
        return try {
            rep.findAllById_MestoContains(mesto.trim()).stream()
                .map { ChastDto(it.id.nomerPodrazdelenia, it.id.tipINomerObedinenia, it.id.nazvanieArmii, it.id.mesto, it.name) }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ChastDto>().stream()
        }
    }
}

@Service
data class ObedinenieService(
    val rep: ObedinenieRepository
) : BaseService<Obedinenie, ObedinenieDto>(rep) {
    override fun getStreamDto() = findAll().map {
        ObedinenieDto(it.id.nomerPodrazdelenia, it.id.tipINomerObedinenia, it.id.nazvanieArmii)
    }.stream()
}

@Service
data class OtdelenieService(
    val rep: OtdelenieRepository
) : BaseService<Otdelenie, OtdelenieDto>(rep) {
    override fun getStreamDto() = findAll().map {
        OtdelenieDto(it.id.nazvOtdel, it.id.nomerPodrazdelenia, it.id.nazvanieArmii, it.id.tipINomerObedinenia, it.id.nazvRoti, it.id.nazvVzvoda, it.id.mesto)
    }.stream()
}

@Service
data class PodrazdelenieService(
    val rep: PodrazdelenieRepository
) : BaseService<Podrazdelenie, PodrazdelenieDto>(rep) {
    override fun getStreamDto() = findAll().map {
        PodrazdelenieDto(it.nomerPodrazdelenia)
    }.stream()
}

@Service
data class RotaService(
    val rep: RotaRepository
) : BaseService<Rota, RotaDto>(rep) {
    override fun getStreamDto() = findAll().map {
        RotaDto(it.id.nazvRoti, it.id.nomerPodrazdelenia, it.id.tipINomerObedinenia, it.id.nazvanieArmii, it.id.mesto)
    }.stream()
}

@Service
data class SoorugeniaService(
    val rep: SoorugeniaRepository
) : BaseService<Soorugenia, SoorugeniaDto>(rep) {
    override fun getStreamDto() = findAll().map {
        SoorugeniaDto(it.id.nomerCoorugenia, it.id.nomerDislocObedinenia, it.id.nomerPodrazdelenia)
    }.stream()

    fun getStreamDtoByPodrazdelenieCount(min: String, max: String): Stream<SoorugeniaAndPodrazdelenieCount> {
        return try {
            val minInt = min.toInt()
            val maxInt = max.toInt()
            val map = mutableMapOf<String, Int>()
            getStreamDto()
                .forEach {
                    if (!map.containsKey(it.nomerCoorugenia.trim())) {
                        map[it.nomerCoorugenia.trim()] = 0
                    }
                    map[it.nomerCoorugenia.trim()] = map[it.nomerCoorugenia.trim()]!! + 1
                }
            map.map {
                SoorugeniaAndPodrazdelenieCount(it.key, it.value.toString())
            }.stream()
                .filter { it.podrazdelenieCount.toInt() in minInt..maxInt }
        } catch (e: Exception) {
            arrayListOf<SoorugeniaAndPodrazdelenieCount>().stream()
        }
    }
}

@Service
data class SostavService(
    val rep: SostavRepository,
    val repChast: ChastRepository
) : BaseService<Sostav, SostavDto>(rep) {
    override fun getStreamDto() = findAll().map {
        SostavDto(it.special, it.tipSostava, it.podchinaetca, it.id.kodSlugashego, it.id.fio, it.id.zvanie, it.id.nomerPodrazdelenia)
    }.stream()

    fun getStreamDtoByZvanie(zvanie: String): Stream<SostavDto> {
        return try {
            rep.findAllById_ZvanieContains(zvanie.trim()).stream()
                .map { SostavDto(it.special, it.tipSostava, it.podchinaetca, it.id.kodSlugashego, it.id.fio, it.id.zvanie, it.id.nomerPodrazdelenia) }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<SostavDto>().stream()
        }
    }

    fun getStreamDtoByNomerPodrazdelenia(nomerPodrazdelenia: String): Stream<SostavDto> {
        return try {
            rep.findAllById_NomerPodrazdeleniaContains(nomerPodrazdelenia.trim())
                .stream()
                .map { SostavDto(it.special, it.tipSostava, it.podchinaetca, it.id.kodSlugashego, it.id.fio, it.id.zvanie, it.id.nomerPodrazdelenia) }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<SostavDto>().stream()
        }
    }

    fun getStreamDtoBySubordination(kod: String): Stream<SostavDto> {
        return try {
            val list = mutableListOf<SostavDto>()
            val sostav = rep.findAll().map { SostavDto(it.special, it.tipSostava, it.podchinaetca, it.id.kodSlugashego, it.id.fio, it.id.zvanie, it.id.nomerPodrazdelenia) }
            getCommander(kod, sostav, list)
            list.stream()
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<SostavDto>().stream()
        }
    }

    fun getStreamDtoWithSpecial(): Stream<SpecialWithCount> {
        return try {
            val map = mutableMapOf<String, Int>()
            getStreamDto()
                .forEach {
                    if (!map.containsKey(it.special.trim())) {
                        map[it.special.trim()] = 0
                    }
                    map[it.special.trim()] = map[it.special.trim()]!! + 1
                }
            map.map {
                SpecialWithCount(it.key, it.value.toString())
            }.stream()
                .filter { it.specialistCount.toInt() >= 5 }
        } catch (e: IndexOutOfBoundsException) {
            return arrayListOf<SpecialWithCount>().stream()
        }
    }

    fun getStreamDtoWithSpecialByArmy(nazvanieArmii: String): Stream<SpecialWithCount> {
        return try {
            val map = mutableMapOf<String, Int>()
            getStreamDto()
                .filter {
                    val chast = repChast.findFirstById_NazvanieArmiiContains(nazvanieArmii.trim())
                    chast.id.nomerPodrazdelenia.trim() == it.nomerPodrazdelenia.trim()
                }
                .forEach {
                    if (!map.containsKey(it.special.trim())) {
                        map[it.special.trim()] = 0
                    }
                    map[it.special.trim()] = map[it.special.trim()]!! + 1
                }
            map.map {
                SpecialWithCount(it.key, it.value.toString())
            }.stream()
                .filter { it.specialistCount.toInt() >= 5 }
        } catch (e: IndexOutOfBoundsException) {
            return arrayListOf<SpecialWithCount>().stream()
        }
    }

    fun getStreamDtoWithSpecialByChast(nazvanieChasti: String): Stream<SpecialWithCount> {
        return try {
            val map = mutableMapOf<String, Int>()
            getStreamDto()
                .filter {
                    val chast = repChast.findFirstByNameContains(nazvanieChasti.trim())
                    chast.id.nomerPodrazdelenia.trim() == it.nomerPodrazdelenia.trim()
                }
                .forEach {
                    if (!map.containsKey(it.special.trim())) {
                        map[it.special.trim()] = 0
                    }
                    map[it.special.trim()] = map[it.special.trim()]!! + 1
                }
            map.map {
                SpecialWithCount(it.key, it.value.toString())
            }.stream()
                .filter { it.specialistCount.toInt() >= 5 }
        } catch (e: IndexOutOfBoundsException) {
            return arrayListOf<SpecialWithCount>().stream()
        }
    }

    private fun getCommander(
        kod: String,
        sostav: List<SostavDto>,
        list: MutableList<SostavDto>
    ) {
        val me = sostav.getByKod(kod) ?: return
        list.add(me)
        if (list.getByKod(me.podchinaetca) != null) {
            return
        }
        getCommander(me.podchinaetca, sostav, list)
    }
}

@Service
data class TehnikaService(
    val rep: TehnikaRepository,
    val repChast: ChastRepository
) : BaseService<Tehnika, TehnikaDto>(rep) {
    override fun getStreamDto() = findAll().map {
        TehnikaDto(it.id.tipTehniki, it.id.kolVoTehniki, it.id.nomerPodrazdelenia)
    }.stream()

    fun getStreamDtoByType(type: String): Stream<ChastAndTehnika> {
        return try {
            rep.findAllById_TipTehnikiContains(type.trim())
                .stream()
                .map { TehnikaDto(it.id.tipTehniki, it.id.kolVoTehniki, it.id.nomerPodrazdelenia) }
                .filter { repChast.findAllById_NomerPodrazdeleniaContains(it.nomerPodrazdelenia).isNotEmpty() }
                .map {
                    val chastElement = repChast.findFirstById_NomerPodrazdeleniaContains(it.nomerPodrazdelenia)
                    ChastAndTehnika(chastElement.name, chastElement.id.nomerPodrazdelenia, chastElement.id.mesto, chastElement.id.tipINomerObedinenia, chastElement.id.nazvanieArmii, it.tipTehniki, it.kolVoTehniki)
                }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ChastAndTehnika>().stream()
        }
    }

    fun getStreamDtoByChast(nazvanieChasti: String): Stream<ChastAndTehnika> {
        return try {
            repChast.findAllByNameContains(nazvanieChasti.trim())
                .stream()
                .filter { rep.findAllById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia).isNotEmpty() }
                .map {
                    val tehnikaElement = rep.findFirstById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia)
                    ChastAndTehnika(it.name, it.id.nomerPodrazdelenia, it.id.mesto, it.id.tipINomerObedinenia, it.id.nazvanieArmii, tehnikaElement.id.tipTehniki, tehnikaElement.id.kolVoTehniki)
                }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ChastAndTehnika>().stream()
        }
    }
}

@Service
data class VoorugenieService(
    val rep: VoorugenieRepository,
    val repChast: ChastRepository
) : BaseService<Voorugenie, VoorugenieDto>(rep) {
    override fun getStreamDto() = findAll().map {
        VoorugenieDto(it.id.tipVoorug, it.id.kolVoVoorug, it.id.nomerPodrazdelenia)
    }.stream()

    fun getStreamDtoByChast(nazvanieChasti: String): Stream<ChastAndVoorugenie> {
        return try {
            repChast.findAllByNameContains(nazvanieChasti.trim())
                .stream()
                .filter { rep.findAllById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia).isNotEmpty() }
                .map {
                    val voorugenieElement = rep.findFirstById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia)
                    ChastAndVoorugenie(it.name, it.id.nomerPodrazdelenia, it.id.mesto, it.id.tipINomerObedinenia, it.id.nazvanieArmii, voorugenieElement.id.tipVoorug, voorugenieElement.id.kolVoVoorug)
                }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ChastAndVoorugenie>().stream()
        }
    }

    fun getStreamDtoByArmy(nazvanieArmii: String): Stream<ChastAndVoorugenie> {
        return try {
            repChast.findAllById_NazvanieArmiiContains(nazvanieArmii.trim())
                .stream()
                .filter { rep.findAllById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia).isNotEmpty() }
                .map {
                    val voorugenieElement = rep.findFirstById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia)
                    ChastAndVoorugenie(it.name, it.id.nomerPodrazdelenia, it.id.mesto, it.id.tipINomerObedinenia, it.id.nazvanieArmii, voorugenieElement.id.tipVoorug, voorugenieElement.id.kolVoVoorug)
                }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ChastAndVoorugenie>().stream()
        }
    }

    fun getStreamDtoByVoorugenieType(tipVoorugenia: String): Stream<ChastAndVoorugenie> {
        return try {
            rep.findAllById_TipVoorugContains(tipVoorugenia.trim())
                .stream()
                .filter { it.id.kolVoVoorug.toInt() >= 10 }
                .map {
                    val chast = repChast.findFirstById_NomerPodrazdeleniaContains(it.id.nomerPodrazdelenia)
                    ChastAndVoorugenie(chast.name, chast.id.nomerPodrazdelenia, chast.id.mesto, chast.id.tipINomerObedinenia, chast.id.nazvanieArmii, it.id.tipVoorug, it.id.kolVoVoorug)
                }
        } catch (e: IndexOutOfBoundsException) {
            arrayListOf<ChastAndVoorugenie>().stream()
        }
    }
}

@Service
data class VzvodService(
    val rep: VzvodRepository
) : BaseService<Vzvod, VzvodDto>(rep) {
    override fun getStreamDto() = findAll().map {
        VzvodDto(it.id.nazvVzvoda, it.id.nomerPodrazdelenia, it.id.nazvanieArmii, it.id.tipINomerObedinenia, it.id.nazvRoti, it.id.mesto)
    }.stream()
}


/* EXTENSION METHODS */

fun List<SostavDto>.getByKod(kod: String): SostavDto? {
    if (this.isEmpty()) {
        return null
    }
    this.forEach {
        if (it.kodSlugashego.trim() == kod.trim()) {
            return it
        }
    }
    return null
}