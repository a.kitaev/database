package com.db.projectdb.backend.model.tehnika

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "tehnika")
data class Tehnika(
    @EmbeddedId
    val id: TehnikaKey
) : BaseEntity