package com.db.projectdb.backend.model.podrazdelenie

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "podrazdelenie")
data class Podrazdelenie(
    @Id
    @Column(name = "Nomer_podrazdelenia")
    val nomerPodrazdelenia: String
) : BaseEntity
