package com.db.projectdb.backend.model.rota

import com.db.projectdb.backend.model.BaseDto

data class RotaDto(
    val nazvRoti: String,
    val nomerPodrazdelenia: String,
    val tipINomerObedinenia: String,
    val nazvanieArmii: String,
    val mesto: String
) : BaseDto