package com.db.projectdb.backend.model.chast

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.Column
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "chast")
data class Chast(
    @EmbeddedId
    var id: ChastKey,

    @Column(name = "Nazvanie_chasti")
    var name: String
) : BaseEntity