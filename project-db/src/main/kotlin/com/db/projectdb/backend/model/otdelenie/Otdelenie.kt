package com.db.projectdb.backend.model.otdelenie

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "otdelenie")
data class Otdelenie(
    @EmbeddedId
    val id: OtdelenieKey
) : BaseEntity