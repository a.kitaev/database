package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.obedinenie.Obedinenie
import com.db.projectdb.backend.model.obedinenie.ObedinenieDto
import com.db.projectdb.backend.model.obedinenie.ObedinenieKey
import com.db.projectdb.backend.service.ObedinenieService
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.router.Route

@Route("obedinenie")
open class ObedinenieScreen(
    service: ObedinenieService
) : BaseTableScreen<Obedinenie, ObedinenieDto>(service) {

    init {
        grid.addColumn(ObedinenieDto::tipINomerObedinenia)
            .setHeader("Тип и номер объединения")
        grid.addColumn(ObedinenieDto::nazvanieArmii)
            .setHeader("Название армии")
        grid.addColumn(ObedinenieDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        this.add(grid)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Тип и номер объединения"),
            TextField("Название армии"),
            TextField("Номер подразделения")
        )
    }

    override fun buildEntity(args: List<String>): Obedinenie {
        return Obedinenie(
            id = ObedinenieKey(
                tipINomerObedinenia = args[0],
                nazvanieArmii = args[1],
                nomerPodrazdelenia = args[2]
            )
        )
    }
}