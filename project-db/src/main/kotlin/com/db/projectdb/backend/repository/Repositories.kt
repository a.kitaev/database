package com.db.projectdb.backend.repository

import com.db.projectdb.backend.model.armia.Armia
import com.db.projectdb.backend.model.chast.Chast
import com.db.projectdb.backend.model.obedinenie.Obedinenie
import com.db.projectdb.backend.model.otdelenie.Otdelenie
import com.db.projectdb.backend.model.podrazdelenie.Podrazdelenie
import com.db.projectdb.backend.model.rota.Rota
import com.db.projectdb.backend.model.soorugenia.Soorugenia
import com.db.projectdb.backend.model.sostav.Sostav
import com.db.projectdb.backend.model.tehnika.Tehnika
import com.db.projectdb.backend.model.voorugenie.Voorugenie
import com.db.projectdb.backend.model.vzvod.Vzvod
import org.springframework.stereotype.Repository

@Repository
interface ArmiaRepository : BaseRepository<Armia>

@Repository
interface ChastRepository : BaseRepository<Chast> {

    fun findFirstByNameContains(nazvanieChasti: String): Chast

    fun findAllById_MestoContains(mesto: String): List<Chast>

    fun findAllById_NomerPodrazdeleniaContains(nomerPodrazdelenia: String): List<Chast>

    fun findFirstById_NomerPodrazdeleniaContains(nomerPodrazdelenia: String): Chast

    fun findAllByNameContains(nazvanieChasti: String): List<Chast>

    fun findAllById_TipINomerObedineniaContains(tipINomerObedinenia: String): List<Chast>

    fun findAllById_NazvanieArmiiContains(nazvanieArmii: String): List<Chast>

    fun findFirstById_NazvanieArmiiContains(nazvanieArmii: String): Chast
}

@Repository
interface ObedinenieRepository : BaseRepository<Obedinenie>

@Repository
interface OtdelenieRepository : BaseRepository<Otdelenie>

@Repository
interface PodrazdelenieRepository : BaseRepository<Podrazdelenie>

@Repository
interface RotaRepository : BaseRepository<Rota>

@Repository
interface SoorugeniaRepository : BaseRepository<Soorugenia>

@Repository
interface SostavRepository : BaseRepository<Sostav> {

    fun findAllById_ZvanieContains(zvanie: String): List<Sostav>

    fun findAllById_NomerPodrazdeleniaContains(nomerPodrazdelenia: String): List<Sostav>

    fun findFirstById_NomerPodrazdeleniaContains(nomerPodrazdelenia: String): Sostav
}

@Repository
interface TehnikaRepository : BaseRepository<Tehnika> {

    fun findAllById_TipTehnikiContains(type: String): List<Tehnika>

    fun findAllById_NomerPodrazdeleniaContains(nomerPodrazdelenia: String): List<Tehnika>

    fun findFirstById_NomerPodrazdeleniaContains(nomerPodrazdelenia: String): Tehnika
}

@Repository
interface VoorugenieRepository : BaseRepository<Voorugenie> {

    fun findAllById_NomerPodrazdeleniaContains(nomerPodrazdelenia: String): List<Voorugenie>

    fun findFirstById_NomerPodrazdeleniaContains(nomerPodrazdelenia: String): Voorugenie

    fun findAllById_TipVoorugContains(tipVoorug: String): List<Voorugenie>

    fun findFirstById_TipVoorug(tipVoorug: String): Voorugenie
}

@Repository
interface VzvodRepository : BaseRepository<Vzvod>