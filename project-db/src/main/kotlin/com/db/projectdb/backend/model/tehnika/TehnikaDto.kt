package com.db.projectdb.backend.model.tehnika

import com.db.projectdb.backend.model.BaseDto

data class TehnikaDto(
    val tipTehniki: String,
    val kolVoTehniki: String,
    val nomerPodrazdelenia: String
) : BaseDto