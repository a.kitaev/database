package com.db.projectdb.backend.model.select_dto

data class ArmyChastCount(
    val nazvanieArmii: String,
    val kolvoChastey: String
)