package com.db.projectdb.backend.model.soorugenia

import com.db.projectdb.backend.model.BaseDto

data class SoorugeniaDto(
    val nomerCoorugenia: String,
    val nomerDislocObedinenia: String,
    val nomerPodrazdelenia: String
) : BaseDto