package com.db.projectdb.backend.model.armia

import com.db.projectdb.backend.model.BaseDto

data class ArmiaDto(
    val nomerPodrazdelenia: String,
    val nazvanieArmii: String
) : BaseDto