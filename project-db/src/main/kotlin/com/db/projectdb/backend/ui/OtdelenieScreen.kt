package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.otdelenie.Otdelenie
import com.db.projectdb.backend.model.otdelenie.OtdelenieDto
import com.db.projectdb.backend.model.otdelenie.OtdelenieKey
import com.db.projectdb.backend.service.OtdelenieService
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.router.Route

@Route("otdelenie")
open class OtdelenieScreen(
    service: OtdelenieService
) : BaseTableScreen<Otdelenie, OtdelenieDto>(service) {

    init {
        grid.addColumn(OtdelenieDto::nazvOtdel)
            .setHeader("Название отделения")
        grid.addColumn(OtdelenieDto::nazvRoti)
            .setHeader("Название роты")
        grid.addColumn(OtdelenieDto::nazvVzvoda)
            .setHeader("Название взвода")
        grid.addColumn(OtdelenieDto::nazvanieArmii)
            .setHeader("Название армии")
        grid.addColumn(OtdelenieDto::mesto)
            .setHeader("Место дислокации")
        grid.addColumn(OtdelenieDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        grid.addColumn(OtdelenieDto::tipINomerObedinenia)
            .setHeader("Тип и номер объединения")
        this.add(grid)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Название отделения"),
            TextField("Название роты"),
            TextField("Название взвода"),
            TextField("Название армии"),
            TextField("Место дислокации"),
            TextField("Номер подразделения"),
            TextField("Тип и номер объединения")
        )
    }

    override fun buildEntity(args: List<String>): Otdelenie {
        return Otdelenie(
            id = OtdelenieKey(
                nazvOtdel = args[0],
                nazvRoti = args[1],
                nazvVzvoda = args[2],
                nazvanieArmii = args[3],
                mesto = args[4],
                nomerPodrazdelenia = args[5],
                tipINomerObedinenia = args[6]
            )
        )
    }
}