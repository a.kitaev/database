package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.rota.Rota
import com.db.projectdb.backend.model.rota.RotaDto
import com.db.projectdb.backend.model.rota.RotaKey
import com.db.projectdb.backend.service.RotaService
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.router.Route

@Route("rota")
open class RotaScreen(
    service: RotaService
) : BaseTableScreen<Rota, RotaDto>(service) {

    init {
        grid.addColumn(RotaDto::nazvRoti)
            .setHeader("Название роты")
        grid.addColumn(RotaDto::nazvanieArmii)
            .setHeader("Название армии")
        grid.addColumn(RotaDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        grid.addColumn(RotaDto::tipINomerObedinenia)
            .setHeader("Тип и номер объединения")
        grid.addColumn(RotaDto::mesto)
            .setHeader("Место дислокации")
        this.add(grid)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Название роты"),
            TextField("Название армии"),
            TextField("Номер подразделения"),
            TextField("Тип и номер объединения"),
            TextField("Место дислокации")
        )
    }

    override fun buildEntity(args: List<String>): Rota {
        return Rota(
            id = RotaKey(
                nazvRoti = args[0],
                nazvanieArmii = args[1],
                nomerPodrazdelenia = args[2],
                tipINomerObedinenia = args[3],
                mesto = args[4]
            )
        )
    }
}