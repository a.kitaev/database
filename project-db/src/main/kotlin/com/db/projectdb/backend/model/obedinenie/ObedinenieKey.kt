package com.db.projectdb.backend.model.obedinenie

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class ObedinenieKey(
    @Column(name = "Nomer_podrazdelenia")
    val nomerPodrazdelenia: String,
    @Column(name = "tip_i_nomer_obedinenia")
    val tipINomerObedinenia: String,
    @Column(name = "nazvanie_armii")
    val nazvanieArmii: String
) : Serializable
