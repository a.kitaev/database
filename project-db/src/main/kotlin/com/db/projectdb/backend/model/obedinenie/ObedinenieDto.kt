package com.db.projectdb.backend.model.obedinenie

import com.db.projectdb.backend.model.BaseDto
import javax.persistence.Embeddable

@Embeddable
data class ObedinenieDto(
    val nomerPodrazdelenia: String,
    val tipINomerObedinenia: String,
    val nazvanieArmii: String
) : BaseDto
