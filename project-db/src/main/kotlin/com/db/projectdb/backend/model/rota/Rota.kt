package com.db.projectdb.backend.model.rota

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "rota")
data class Rota(
    @EmbeddedId
    val id: RotaKey
) : BaseEntity