package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.select_dto.SpecialWithCount
import com.db.projectdb.backend.model.sostav.Sostav
import com.db.projectdb.backend.model.sostav.SostavDto
import com.db.projectdb.backend.model.sostav.SostavKey
import com.db.projectdb.backend.service.SostavService
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.router.Route

@Route("sostav")
open class SostavScreen(
    service: SostavService
) : BaseTableScreen<Sostav, SostavDto>(service) {

    val gridForSelect = Grid<SostavDto>()
    val gridForSelect2 = Grid<SpecialWithCount>()

    init {
        grid.addColumn(SostavDto::kodSlugashego)
            .setHeader("Код служащего")
        grid.addColumn(SostavDto::fio)
            .setHeader("ФИО")
        grid.addColumn(SostavDto::special)
            .setHeader("Специальность")
        grid.addColumn(SostavDto::zvanie)
            .setHeader("Звание")
        grid.addColumn(SostavDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        grid.addColumn(SostavDto::podchinaetca)
            .setHeader("Командир")
        grid.addColumn(SostavDto::tipSostava)
            .setHeader("Тип состава")
        this.add(grid)

        // text field for select
        val textField = TextField()
        this.add(textField)

        val selectArea = HorizontalLayout().apply {
            add(ButtonUtil.generateButtonWithField("Состав по званию", textField) {
                val dataProvider: DataProvider<SostavDto, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByZvanie(textField.value)
                }, {
                    service.getStreamDtoByZvanie(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })

            add(ButtonUtil.generateButtonWithField("Цепочка подчинённости по коду", textField) {
                val dataProvider: DataProvider<SostavDto, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoBySubordination(textField.value)
                }, {
                    service.getStreamDtoBySubordination(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })

            add(ButtonUtil.generateButtonWithField("Состав по номеру подразделения", textField) {
                val dataProvider: DataProvider<SostavDto, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByNomerPodrazdelenia(textField.value)
                }, {
                    service.getStreamDtoByNomerPodrazdelenia(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })
        }

        gridForSelect.addColumn(SostavDto::kodSlugashego)
            .setHeader("Код служащего")
        gridForSelect.addColumn(SostavDto::fio)
            .setHeader("ФИО")
        gridForSelect.addColumn(SostavDto::special)
            .setHeader("Специальность")
        gridForSelect.addColumn(SostavDto::zvanie)
            .setHeader("Звание")
        gridForSelect.addColumn(SostavDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        gridForSelect.addColumn(SostavDto::podchinaetca)
            .setHeader("Командир")
        gridForSelect.addColumn(SostavDto::tipSostava)
            .setHeader("Тип состава")
        this.add(selectArea)
        this.add(gridForSelect)

        // text field for second select
        val textField2 = TextField()
        this.add(textField2)

        val selectArea2 = HorizontalLayout().apply {
            add(Button("Специальности с >5 специалистами").apply {
                val dataProvider: DataProvider<SpecialWithCount, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoWithSpecial()
                }, {
                    service.getStreamDtoWithSpecial().count().toInt()
                })
                gridForSelect2.dataProvider = dataProvider
            })

            add(ButtonUtil.generateButtonWithField("C >5 специалистами по армии", textField2) {
                val dataProvider: DataProvider<SpecialWithCount, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoWithSpecialByArmy(textField.value)
                }, {
                    service.getStreamDtoWithSpecialByArmy(textField.value).count().toInt()
                })
                gridForSelect2.dataProvider = dataProvider
            })

            add(ButtonUtil.generateButtonWithField("C >5 специалистами по часте", textField2) {
                val dataProvider: DataProvider<SpecialWithCount, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoWithSpecialByChast(textField.value)
                }, {
                    service.getStreamDtoWithSpecialByChast(textField.value).count().toInt()
                })
                gridForSelect2.dataProvider = dataProvider
            })
        }

        gridForSelect2.addColumn(SpecialWithCount::special)
            .setHeader("Специальность")
        gridForSelect2.addColumn(SpecialWithCount::specialistCount)
            .setHeader("Число специалистов")
        this.add(selectArea2)
        this.add(gridForSelect2)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Код служащего"),
            TextField("ФИО"),
            TextField("Специальность"),
            TextField("Звание"),
            TextField("Номер подразделения"),
            TextField("Командир"),
            TextField("Тип состава")
        )
    }

    override fun buildEntity(args: List<String>): Sostav {
        return Sostav(
            id = SostavKey(
                kodSlugashego = args[0],
                fio = args[1],
                zvanie = args[3],
                nomerPodrazdelenia = args[4]
            ),
            special = args[2],
            podchinaetca = args[5],
            tipSostava = args[6]
        )
    }
}