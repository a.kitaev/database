package com.db.projectdb.backend.model.tehnika

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class TehnikaKey(
    @Column(name = "Tip_tehniki")
    val tipTehniki: String,
    @Column(name = "Kol_vo_tehniki")
    val kolVoTehniki: String,
    @Column(name = "Nomer_podrazdelenia")
    val nomerPodrazdelenia: String
) : Serializable