package com.db.projectdb.backend.ui

import com.vaadin.flow.router.Route

@Route("home")
open class MainScreen : BaseNavigationScreen()