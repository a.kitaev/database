package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.armia.Armia
import com.db.projectdb.backend.model.armia.ArmiaDto
import com.db.projectdb.backend.model.armia.ArmiaKey
import com.db.projectdb.backend.model.select_dto.ArmyChastCount
import com.db.projectdb.backend.service.ArmiaService
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.router.Route

@Route("armia")
open class ArmiaScreen(
    service: ArmiaService
) : BaseTableScreen<Armia, ArmiaDto>(service) {

    val gridForSelect = Grid<ArmyChastCount>()

    init {
        grid.addColumn(ArmiaDto::nazvanieArmii)
            .setHeader("Название армии")
        grid.addColumn(ArmiaDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        this.add(grid)

        val selectArea = HorizontalLayout().apply {
            add(Button("Армия с максимальным числом частей").apply {
                addClickListener {
                    val dataProvider: DataProvider<ArmyChastCount, Void> = DataProvider.fromCallbacks({
                        val offset = it.offset
                        val limit = it.limit
                        service.getStreamDtoMaxChast()
                    }, {
                        service.getStreamDtoMaxChast().count().toInt()
                    })
                    gridForSelect.dataProvider = dataProvider
                }
            })

            add(Button("Армия с минимальным числом частей").apply {
                addClickListener {
                    val dataProvider: DataProvider<ArmyChastCount, Void> = DataProvider.fromCallbacks({
                        val offset = it.offset
                        val limit = it.limit
                        service.getStreamDtoMinChast()
                    }, {
                        service.getStreamDtoMinChast().count().toInt()
                    })
                    gridForSelect.dataProvider = dataProvider
                }
            })
        }

        gridForSelect.addColumn(ArmyChastCount::nazvanieArmii)
            .setHeader("Название армии")
        gridForSelect.addColumn(ArmyChastCount::kolvoChastey)
            .setHeader("Количество частей")
        this.add(selectArea)
        this.add(gridForSelect)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Номер подразделения"),
            TextField("Название армии")
        )
    }

    override fun buildEntity(args: List<String>): Armia {
        return Armia(
            ArmiaKey(
                nomerPodrazdelenia = args[0],
                nazvanieArmii = args[1]
            )
        )
    }
}