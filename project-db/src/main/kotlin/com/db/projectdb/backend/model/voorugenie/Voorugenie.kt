package com.db.projectdb.backend.model.voorugenie

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "voorugenie")
data class Voorugenie(
    @EmbeddedId
    val id: VoorugenieKey
) : BaseEntity