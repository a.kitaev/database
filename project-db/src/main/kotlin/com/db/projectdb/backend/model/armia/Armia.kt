package com.db.projectdb.backend.model.armia

import com.db.projectdb.backend.model.BaseEntity
import org.springframework.data.repository.NoRepositoryBean
import java.beans.BeanProperty
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "armia")
data class Armia(
    @EmbeddedId
    var id: ArmiaKey
) : BaseEntity