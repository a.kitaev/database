package com.db.projectdb.backend.model.armia

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class ArmiaKey(
    @Column(name = "Nomer_podrazdelenia")
    val nomerPodrazdelenia: String,
    @Column(name = "nazvanie_armii")
    val nazvanieArmii: String
) : Serializable