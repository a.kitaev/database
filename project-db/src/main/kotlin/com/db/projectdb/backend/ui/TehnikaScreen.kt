package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.select_dto.ChastAndTehnika
import com.db.projectdb.backend.model.tehnika.Tehnika
import com.db.projectdb.backend.model.tehnika.TehnikaDto
import com.db.projectdb.backend.model.tehnika.TehnikaKey
import com.db.projectdb.backend.service.TehnikaService
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.router.Route

@Route("tehnika")
open class TehnikaScreen(
    service: TehnikaService
) : BaseTableScreen<Tehnika, TehnikaDto>(service) {

    val gridForSelect = Grid<ChastAndTehnika>()

    init {
        grid.addColumn(TehnikaDto::tipTehniki)
            .setHeader("Тип техники")
        grid.addColumn(TehnikaDto::kolVoTehniki)
            .setHeader("Количество единиц техники")
        grid.addColumn(TehnikaDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        this.add(grid)

        // text field for select
        val textField = TextField()
        this.add(textField)

        val selectArea = HorizontalLayout().apply {
            add(ButtonUtil.generateButtonWithField("Техника по типу", textField) {
                val dataProvider: DataProvider<ChastAndTehnika, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByType(textField.value)
                }, {
                    service.getStreamDtoByType(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })

            add(ButtonUtil.generateButtonWithField("Техника по части", textField) {
                val dataProvider: DataProvider<ChastAndTehnika, Void> = DataProvider.fromCallbacks({
                    val offset = it.offset
                    val limit = it.limit
                    service.getStreamDtoByChast(textField.value)
                }, {
                    service.getStreamDtoByChast(textField.value).count().toInt()
                })
                gridForSelect.dataProvider = dataProvider
            })
        }

        gridForSelect.addColumn(ChastAndTehnika::tipTehniki)
            .setHeader("Тип техники")
        gridForSelect.addColumn(ChastAndTehnika::kolVoTehniki)
            .setHeader("Количество техники")
        gridForSelect.addColumn(ChastAndTehnika::nazvanieChasti)
            .setHeader("Название части")
        gridForSelect.addColumn(ChastAndTehnika::mesto)
            .setHeader("Место дислокации")
        gridForSelect.addColumn(ChastAndTehnika::nazvanieArmii)
            .setHeader("Название армии")
        gridForSelect.addColumn(ChastAndTehnika::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        gridForSelect.addColumn(ChastAndTehnika::tipINomerObedinenia)
            .setHeader("Тип и номер объединения")
        this.add(selectArea)
        this.add(gridForSelect)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Тип техники"),
            TextField("Количество единиц техники"),
            TextField("Номер подразделения")
        )
    }

    override fun buildEntity(args: List<String>): Tehnika {
        return Tehnika(
            id = TehnikaKey(
                tipTehniki = args[0],
                kolVoTehniki = args[1],
                nomerPodrazdelenia = args[2]
            )
        )
    }
}