package com.db.projectdb.backend.model.otdelenie

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class OtdelenieKey(
    @Column(name = "Nazv_otdel")
    val nazvOtdel: String,
    @Column(name = "Nomer_podrazdelenia")
    val nomerPodrazdelenia: String,
    @Column(name = "nazvanie_armii")
    val nazvanieArmii: String,
    @Column(name = "tip_i_nomer_obedinenia")
    val tipINomerObedinenia: String,
    @Column(name = "Nazv_roti")
    val nazvRoti: String,
    @Column(name = "Nazv_vzvoda")
    val nazvVzvoda: String,
    @Column(name = "Mesto")
    val mesto: String
) : Serializable