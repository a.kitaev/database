package com.db.projectdb.backend.model.vzvod

import com.db.projectdb.backend.model.BaseDto

data class VzvodDto(
    val nazvVzvoda: String,
    val nomerPodrazdelenia: String,
    val nazvanieArmii: String,
    val tipINomerObedinenia: String,
    val nazvRoti: String,
    val mesto: String
) : BaseDto