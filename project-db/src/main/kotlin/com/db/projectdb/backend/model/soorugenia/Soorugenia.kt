package com.db.projectdb.backend.model.soorugenia

import com.db.projectdb.backend.model.BaseEntity
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "soorugenia")
data class Soorugenia(
    @EmbeddedId
    val id: SoorugeniaKey
) : BaseEntity