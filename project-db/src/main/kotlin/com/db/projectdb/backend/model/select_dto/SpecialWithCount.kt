package com.db.projectdb.backend.model.select_dto

data class SpecialWithCount(
    val special: String,
    val specialistCount: String
)