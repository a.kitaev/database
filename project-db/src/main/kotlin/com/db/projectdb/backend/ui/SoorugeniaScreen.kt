package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.select_dto.SoorugeniaAndPodrazdelenieCount
import com.db.projectdb.backend.model.soorugenia.Soorugenia
import com.db.projectdb.backend.model.soorugenia.SoorugeniaDto
import com.db.projectdb.backend.model.soorugenia.SoorugeniaKey
import com.db.projectdb.backend.service.SoorugeniaService
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.router.Route

@Route("soorugenia")
open class SoorugeniaScreen(
    service: SoorugeniaService
) : BaseTableScreen<Soorugenia, SoorugeniaDto>(service) {

    val gridForSelect = Grid<SoorugeniaAndPodrazdelenieCount>()

    init {
        grid.addColumn(SoorugeniaDto::nomerCoorugenia)
            .setHeader("Номер сооружения")
        grid.addColumn(SoorugeniaDto::nomerDislocObedinenia)
            .setHeader("Номер дислоцирующегося объединения")
        grid.addColumn(SoorugeniaDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        this.add(grid)

        // text field for select
        val horLayout = HorizontalLayout()
        val textFieldLeft = TextField("Минимальное число подразделений")
        val textFieldRight = TextField("Максимальное число подразделений")
        horLayout.add(textFieldLeft)
        horLayout.add(textFieldRight)
        this.add(horLayout)

        val selectArea = HorizontalLayout().apply {
            add(Button("Сооружения по кол-ву подразделений").apply {
                addClickListener {
                    val dataProvider: DataProvider<SoorugeniaAndPodrazdelenieCount, Void> = DataProvider.fromCallbacks({
                        val offset = it.offset
                        val limit = it.limit
                        service.getStreamDtoByPodrazdelenieCount(textFieldLeft.value, textFieldRight.value)
                    }, {
                        service.getStreamDtoByPodrazdelenieCount(textFieldLeft.value, textFieldRight.value).count().toInt()
                    })
                    gridForSelect.dataProvider = dataProvider
                }
            })
        }

        gridForSelect.addColumn(SoorugeniaAndPodrazdelenieCount::nomerSoorugenia)
            .setHeader("Номер сооружения")
        gridForSelect.addColumn(SoorugeniaAndPodrazdelenieCount::podrazdelenieCount)
            .setHeader("Число дислоцируемых подразделений")
        this.add(selectArea)
        this.add(gridForSelect)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Номер сооружения"),
            TextField("Номер дислоцирующегося объединения"),
            TextField("Номер подразделения")
        )
    }

    override fun buildEntity(args: List<String>): Soorugenia {
        return Soorugenia(
            id = SoorugeniaKey(
                nomerCoorugenia = args[0],
                nomerDislocObedinenia = args[1],
                nomerPodrazdelenia = args[2]
            )
        )
    }
}