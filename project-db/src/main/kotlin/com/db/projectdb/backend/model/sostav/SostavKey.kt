package com.db.projectdb.backend.model.sostav

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class SostavKey(
    @Column(name = "Kod_slugashego")
    val kodSlugashego: String,
    @Column(name = "FIO")
    val fio: String,
    @Column(name = "Zvanie")
    val zvanie: String,
    @Column(name = "Nomer_podrazdelenia")
    val nomerPodrazdelenia: String
) : Serializable