package com.db.projectdb.backend.service

import com.db.projectdb.backend.model.BaseDto
import com.db.projectdb.backend.model.BaseEntity
import com.db.projectdb.backend.repository.BaseRepository
import java.util.stream.Stream

abstract class BaseService<T : BaseEntity, D : BaseDto>(
    private val rep: BaseRepository<T>
) {

    fun findById(id: String): T {
        return rep.getOne(id)
    }

    fun save(entity: T): T {
        return rep.save(entity)
    }

    fun delete(entity: T) {
        rep.delete(entity)
    }

    fun findAll(): MutableList<T> {
        return rep.findAll()
    }

    abstract fun getStreamDto(): Stream<D>
}
