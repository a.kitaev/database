package com.db.projectdb.backend.model.soorugenia

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class SoorugeniaKey(
    @Column(name = "Nomer_coorugenia")
    val nomerCoorugenia: String,
    @Column(name = "Nomer_disloc_obedinenia")
    val nomerDislocObedinenia: String,
    @Column(name = "Nomer_podrazdelenia")
    val nomerPodrazdelenia: String
) : Serializable