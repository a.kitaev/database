package com.db.projectdb.backend.model.select_dto

data class SoorugeniaAndPodrazdelenieCount(
    val nomerSoorugenia: String,
    val podrazdelenieCount: String
)