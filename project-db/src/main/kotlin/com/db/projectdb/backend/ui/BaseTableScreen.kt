package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.BaseDto
import com.db.projectdb.backend.model.BaseEntity
import com.db.projectdb.backend.service.BaseService
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.DataProvider

abstract class BaseTableScreen<T : BaseEntity, D : BaseDto>(
    private val service: BaseService<T, D>
) : BaseNavigationScreen() {

    val grid: Grid<D> = Grid()
    var addTextFields: List<TextField> = mutableListOf()

    init {
        this.add(grid)
        updateMainTable()

        addTextFields = getAddFields()
        val horizontalTextField = HorizontalLayout()
        addTextFields.forEach {
            horizontalTextField.add(it)
        }

        val horizontalButtons = HorizontalLayout()
        val addOrUpdateEntityButton = Button("Добавить/обновить сущность").apply {
            addClickListener {
                addTextFields.forEach {
                    if (it.value.isEmpty()) {
                        return@addClickListener
                    }
                }
                service.save(buildEntity(addTextFields.map { it.value.take(10).trim() }))
                updateMainTable()
            }
        }
        horizontalButtons.add(addOrUpdateEntityButton)

        val removeEntityButton = Button("Удалить сущность").apply {
            addClickListener {
                addTextFields.forEach {
                    if (it.value.isEmpty()) {
                        return@addClickListener
                    }
                }
                service.delete(buildEntity(addTextFields.map { it.value.take(10).trim() }))
                updateMainTable()
            }
        }
        horizontalButtons.add(removeEntityButton)

        this.add(horizontalTextField)
        this.add(horizontalButtons)
    }

    private fun updateMainTable() {
        val dataProvider: DataProvider<D, Void> = DataProvider.fromCallbacks({
            val offset = it.offset
            val limit = it.limit
            service.getStreamDto()
        }, {
            service.findAll().size
        })
        grid.dataProvider = dataProvider
    }

    abstract fun getAddFields(): List<TextField>
    abstract fun buildEntity(args: List<String>): T
}