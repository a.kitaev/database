package com.db.projectdb.backend.model.sostav

import com.db.projectdb.backend.model.BaseDto

data class SostavDto(
    val special: String,
    val tipSostava: String,
    val podchinaetca: String,
    val kodSlugashego: String,
    val fio: String,
    val zvanie: String,
    val nomerPodrazdelenia: String
) : BaseDto