package com.db.projectdb.backend.ui

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.textfield.TextField

object ButtonUtil {

    fun generateTableButton(page: String): Button {
        return generateButton("$page table", page)
    }

    fun generateButton(title: String, page: String): Button {
        return Button(title).apply {
            addClickListener {
                ui.ifPresent { it.navigate(page) }
            }
        }
    }

    fun generateButtonWithField(
        title: String,
        textField: TextField,
        callback: (TextField) -> Unit
    ): Button {
        return Button(title).apply {
            addClickListener {
                if (!textField.isEmpty) {
                    callback.invoke(textField)
                }
            }
        }
    }
}