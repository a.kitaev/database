package com.db.projectdb.backend.model.select_dto

data class ChastAndTehnika(
    val nazvanieChasti: String,
    val nomerPodrazdelenia: String,
    val mesto: String,
    val tipINomerObedinenia: String,
    val nazvanieArmii: String,
    val tipTehniki: String,
    val kolVoTehniki: String
)
