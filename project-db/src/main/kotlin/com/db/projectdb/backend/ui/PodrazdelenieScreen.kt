package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.podrazdelenie.Podrazdelenie
import com.db.projectdb.backend.model.podrazdelenie.PodrazdelenieDto
import com.db.projectdb.backend.service.PodrazdelenieService
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.router.Route

@Route("podrazdelenie")
open class PodrazdelenieScreen(
    service: PodrazdelenieService
) : BaseTableScreen<Podrazdelenie, PodrazdelenieDto>(service) {

    init {
        grid.addColumn(PodrazdelenieDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        this.add(grid)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Номер подразделения")
        )
    }

    override fun buildEntity(args: List<String>): Podrazdelenie {
        return Podrazdelenie(
            nomerPodrazdelenia = args[0]
        )
    }
}