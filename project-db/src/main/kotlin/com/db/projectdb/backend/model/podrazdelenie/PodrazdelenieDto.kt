package com.db.projectdb.backend.model.podrazdelenie

import com.db.projectdb.backend.model.BaseDto

data class PodrazdelenieDto(
    val nomerPodrazdelenia: String
) : BaseDto