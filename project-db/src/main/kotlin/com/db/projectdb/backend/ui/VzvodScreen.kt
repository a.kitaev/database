package com.db.projectdb.backend.ui

import com.db.projectdb.backend.model.vzvod.Vzvod
import com.db.projectdb.backend.model.vzvod.VzvodDto
import com.db.projectdb.backend.model.vzvod.VzvodKey
import com.db.projectdb.backend.service.VzvodService
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.router.Route

@Route("vzvod")
open class VzvodScreen(
    service: VzvodService
) : BaseTableScreen<Vzvod, VzvodDto>(service) {

    init {
        grid.addColumn(VzvodDto::nazvVzvoda)
            .setHeader("Название взвода")
        grid.addColumn(VzvodDto::mesto)
            .setHeader("Место дислокации")
        grid.addColumn(VzvodDto::nazvRoti)
            .setHeader("Название роты")
        grid.addColumn(VzvodDto::nazvanieArmii)
            .setHeader("Название армии")
        grid.addColumn(VzvodDto::nomerPodrazdelenia)
            .setHeader("Номер подразделения")
        grid.addColumn(VzvodDto::tipINomerObedinenia)
            .setHeader("Тип и номер объединения")
        this.add(grid)
    }

    override fun getAddFields(): List<TextField> {
        return arrayListOf(
            TextField("Название взвода"),
            TextField("Место дислокации"),
            TextField("Название роты"),
            TextField("Название армии"),
            TextField("Номер подразделения"),
            TextField("Тип и номер объединения")
        )
    }

    override fun buildEntity(args: List<String>): Vzvod {
        return Vzvod(
            id = VzvodKey(
                nazvVzvoda = args[0],
                mesto = args[1],
                nazvRoti = args[2],
                nazvanieArmii = args[3],
                nomerPodrazdelenia = args[4],
                tipINomerObedinenia = args[5]
            )
        )
    }
}