package com.db.projectdb.backend.ui

import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout

abstract class BaseNavigationScreen : VerticalLayout() {

    init {
        val horizontalLayout = HorizontalLayout()
        horizontalLayout.add(ButtonUtil.generateTableButton("armia"))
        horizontalLayout.add(ButtonUtil.generateTableButton("chast"))
        horizontalLayout.add(ButtonUtil.generateTableButton("obedinenie"))
        horizontalLayout.add(ButtonUtil.generateTableButton("otdelenie"))
        horizontalLayout.add(ButtonUtil.generateTableButton("podrazdelenie"))
        horizontalLayout.add(ButtonUtil.generateTableButton("rota"))
        horizontalLayout.add(ButtonUtil.generateTableButton("soorugenia"))
        horizontalLayout.add(ButtonUtil.generateTableButton("sostav"))
        horizontalLayout.add(ButtonUtil.generateTableButton("tehnika"))
        horizontalLayout.add(ButtonUtil.generateTableButton("voorugenie"))
        horizontalLayout.add(ButtonUtil.generateTableButton("vzvod"))
        this.add(horizontalLayout)
    }
}