package com.db.projectdb.backend.repository

import com.db.projectdb.backend.model.BaseEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface BaseRepository<E : BaseEntity> : JpaRepository<E, String>