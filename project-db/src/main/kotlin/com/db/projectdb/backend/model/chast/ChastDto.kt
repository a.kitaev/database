package com.db.projectdb.backend.model.chast

import com.db.projectdb.backend.model.BaseDto

data class ChastDto(
    val nomerPodrazdelenia: String,
    val tipINomerObedinenia: String,
    val nazvanieArmii: String,
    val mesto: String,
    val name: String
) : BaseDto