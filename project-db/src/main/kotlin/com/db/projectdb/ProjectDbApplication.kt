package com.db.projectdb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class ProjectDbApplication

fun main(args: Array<String>) {
    runApplication<ProjectDbApplication>(*args)
}
