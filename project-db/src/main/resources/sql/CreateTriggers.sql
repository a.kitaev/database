Create trigger [tu_Chast]
    on [Chast]
    for update as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if update([Nomer_podrazdelenia]) or
       update([tip_i_nomer_obedinenia]) or
       update([nazvanie_armii]) or
       update([Mesto])
        begin
            if exists(select 1
                      from [Rota] t,
                           deleted d
                      where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia]
                        and t.[tip_i_nomer_obedinenia] = d.[tip_i_nomer_obedinenia]
                        and t.[nazvanie_armii] = d.[nazvanie_armii]
                        and t.[Mesto] = d.[Mesto])
                begin
                    rollback transaction
                    return
                end
        end
end
go


Create trigger [tu_Rota]
    on [Rota]
    for update as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if update([Nazv_roti]) or
       update([Nomer_podrazdelenia]) or
       update([tip_i_nomer_obedinenia]) or
       update([nazvanie_armii]) or
       update([Mesto])
        begin
            if exists(select 1
                      from [Vzvod] t,
                           deleted d
                      where t.[Nazv_roti] = d.[Nazv_roti]
                        and t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia]
                        and t.[tip_i_nomer_obedinenia] = d.[tip_i_nomer_obedinenia]
                        and t.[nazvanie_armii] = d.[nazvanie_armii]
                        and t.[Mesto] = d.[Mesto])
                begin
                    rollback transaction
                    return
                end
        end
end
go


Create trigger [tu_Vzvod]
    on [Vzvod]
    for update as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if update([Nazv_vzvoda]) or
       update([Nomer_podrazdelenia]) or
       update([nazvanie_armii]) or
       update([tip_i_nomer_obedinenia]) or
       update([Nazv_roti]) or
       update([Mesto])
        begin
            if exists(select 1
                      from [Otdelenie] t,
                           deleted d
                      where t.[Nazv_vzvoda] = d.[Nazv_vzvoda]
                        and t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia]
                        and t.[nazvanie_armii] = d.[nazvanie_armii]
                        and t.[tip_i_nomer_obedinenia] = d.[tip_i_nomer_obedinenia]
                        and t.[Nazv_roti] = d.[Nazv_roti]
                        and t.[Mesto] = d.[Mesto])
                begin
                    rollback transaction
                    return
                end
        end
end
go


Create trigger [tu_Obedinenie]
    on [Obedinenie]
    for update as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if update([tip_i_nomer_obedinenia]) or
       update([Nomer_podrazdelenia]) or
       update([nazvanie_armii])
        begin
            if exists(select 1
                      from [Chast] t,
                           deleted d
                      where t.[tip_i_nomer_obedinenia] = d.[tip_i_nomer_obedinenia]
                        and t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia]
                        and t.[nazvanie_armii] = d.[nazvanie_armii])
                begin
                    rollback transaction
                    return
                end
        end
end
go


Create trigger [tu_armia]
    on [Armia]
    for update as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if update([nazvanie_armii]) or
       update([Nomer_podrazdelenia])
        begin
            if exists(select 1
                      from [Obedinenie] t,
                           deleted d
                      where t.[nazvanie_armii] = d.[nazvanie_armii]
                        and t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
                begin
                    rollback transaction
                    return
                end
        end
end
go


Create trigger [tu_Podrazdelenie]
    on [Podrazdelenie]
    for update as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if update([Nomer_podrazdelenia])
        begin
            if exists(select 1
                      from [Armia] t,
                           deleted d
                      where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
                begin
                    rollback transaction
                    return
                end
        end

    if update([Nomer_podrazdelenia])
        begin
            if exists(select 1
                      from [Sostav] t,
                           deleted d
                      where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
                begin
                    rollback transaction
                    return
                end
        end

    if update([Nomer_podrazdelenia])
        begin
            if exists(select 1
                      from [Tehnika] t,
                           deleted d
                      where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
                begin
                    rollback transaction
                    return
                end
        end

    if update([Nomer_podrazdelenia])
        begin
            if exists(select 1
                      from [Voorugenie] t,
                           deleted d
                      where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
                begin
                    rollback transaction
                    return
                end
        end

    if update([Nomer_podrazdelenia])
        begin
            if exists(select 1
                      from [Soorugenia] t,
                           deleted d
                      where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
                begin
                    rollback transaction
                    return
                end
        end
end
go


Create trigger [td_Chast]
    on [Chast]
    for delete as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if exists(select 1
              from [Rota] t,
                   deleted d
              where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia]
                and t.[tip_i_nomer_obedinenia] = d.[tip_i_nomer_obedinenia]
                and t.[nazvanie_armii] = d.[nazvanie_armii]
                and t.[Mesto] = d.[Mesto])
        begin
            rollback transaction
            return
        end
end
go


Create trigger [td_Rota]
    on [Rota]
    for delete as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if exists(select 1
              from [Vzvod] t,
                   deleted d
              where t.[Nazv_roti] = d.[Nazv_roti]
                and t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia]
                and t.[tip_i_nomer_obedinenia] = d.[tip_i_nomer_obedinenia]
                and t.[nazvanie_armii] = d.[nazvanie_armii]
                and t.[Mesto] = d.[Mesto])
        begin
            rollback transaction
            return
        end
end
go


Create trigger [td_Vzvod]
    on [Vzvod]
    for delete as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if exists(select 1
              from [Otdelenie] t,
                   deleted d
              where t.[Nazv_vzvoda] = d.[Nazv_vzvoda]
                and t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia]
                and t.[nazvanie_armii] = d.[nazvanie_armii]
                and t.[tip_i_nomer_obedinenia] = d.[tip_i_nomer_obedinenia]
                and t.[Nazv_roti] = d.[Nazv_roti]
                and t.[Mesto] = d.[Mesto])
        begin
            rollback transaction
            return
        end
end
go


Create trigger [td_Obedinenie]
    on [Obedinenie]
    for delete as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if exists(select 1
              from [Chast] t,
                   deleted d
              where t.[tip_i_nomer_obedinenia] = d.[tip_i_nomer_obedinenia]
                and t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia]
                and t.[nazvanie_armii] = d.[nazvanie_armii])
        begin
            rollback transaction
            return
        end
end
go


Create trigger [td_armia]
    on [armia]
    for delete as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if exists(select 1
              from [Obedinenie] t,
                   deleted d
              where t.[nazvanie_armii] = d.[nazvanie_armii]
                and t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
        begin
            rollback transaction
            return
        end
end
go


Create trigger [td_Podrazdelenie]
    on [Podrazdelenie]
    for delete as
begin
    declare
        @numrows int
    select @numrows = @@rowcount
    if @numrows = 0
        return

    if exists(select 1
              from [Armia] t,
                   deleted d
              where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
        begin
            rollback transaction
            return
        end

    if exists(select 1
              from [sostav] t,
                   deleted d
              where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
        begin
            rollback transaction
            return
        end

    if exists(select 1
              from [Tehnika] t,
                   deleted d
              where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
        begin
            rollback transaction
            return
        end

    if exists(select 1
              from [Voorugenie] t,
                   deleted d
              where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
        begin
            rollback transaction
            return
        end

    if exists(select 1
              from [Soorugenia] t,
                   deleted d
              where t.[Nomer_podrazdelenia] = d.[Nomer_podrazdelenia])
        begin
            rollback transaction
            return
        end
end
go