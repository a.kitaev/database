Create table [Chast]
(
    [Nazvanie_chasti]        Char(10) NULL,
    [Nomer_podrazdelenia]    Char(10) NOT NULL,
    [tip_i_nomer_obedinenia] Char(10) NOT NULL,
    [nazvanie_armii]         Char(10) NOT NULL,
    [Mesto]                  Char(10) NOT NULL,
    Constraint [pk_Chast] Primary Key ([Nomer_podrazdelenia], [tip_i_nomer_obedinenia], [nazvanie_armii], [Mesto])
)
go

Create table [Rota]
(
    [Nazv_roti]              Char(10) NOT NULL,
    [Nomer_podrazdelenia]    Char(10) NOT NULL,
    [tip_i_nomer_obedinenia] Char(10) NOT NULL,
    [nazvanie_armii]         Char(10) NOT NULL,
    [Mesto]                  Char(10) NOT NULL,
    Constraint [pk_Rota] Primary Key ([Nazv_roti], [Nomer_podrazdelenia], [tip_i_nomer_obedinenia], [nazvanie_armii],
                                      [Mesto])
)
go

Create table [Vzvod]
(
    [Nazv_vzvoda]            Char(10) NOT NULL,
    [Nomer_podrazdelenia]    Char(10) NOT NULL,
    [nazvanie_armii]         Char(10) NOT NULL,
    [tip_i_nomer_obedinenia] Char(10) NOT NULL,
    [Nazv_roti]              Char(10) NOT NULL,
    [Mesto]                  Char(10) NOT NULL,
    Constraint [pk_Vzvod] Primary Key ([Nazv_vzvoda], [Nomer_podrazdelenia], [nazvanie_armii], [tip_i_nomer_obedinenia],
                                       [Nazv_roti], [Mesto])
)
go

Create table [Otdelenie]
(
    [Nazv_otdel]             Char(10) NOT NULL,
    [Nomer_podrazdelenia]    Char(10) NOT NULL,
    [nazvanie_armii]         Char(10) NOT NULL,
    [tip_i_nomer_obedinenia] Char(10) NOT NULL,
    [Nazv_roti]              Char(10) NOT NULL,
    [Nazv_vzvoda]            Char(10) NOT NULL,
    [Mesto]                  Char(10) NOT NULL,
    Constraint [pk_Otdelenie] Primary Key ([Nazv_otdel], [Nomer_podrazdelenia], [nazvanie_armii],
                                           [tip_i_nomer_obedinenia], [Nazv_roti], [Nazv_vzvoda], [Mesto])
)
go

Create table [Obedinenie]
(
    [tip_i_nomer_obedinenia] Char(10) NOT NULL,
    [Nomer_podrazdelenia]    Char(10) NOT NULL,
    [nazvanie_armii]         Char(10) NOT NULL,
    Constraint [pk_Obedinenie] Primary Key ([tip_i_nomer_obedinenia], [Nomer_podrazdelenia], [nazvanie_armii])
)
go

Create table [Sostav]
(
    [Kod_slugashego]      Char(10) NOT NULL,
    [FIO]                 Char(30) NOT NULL,
    [Zvanie]              Char(30) NOT NULL,
    [Special]             Char(30) NOT NULL,
    [Tip_sostava]         Char(10) NOT NULL,
    [Podchinaetca]        Char(10) NOT NULL,
    [Nomer_podrazdelenia] Char(10) NOT NULL,
    Constraint [pk_sostav] Primary Key ([Kod_slugashego], [FIO], [Zvanie], [Nomer_podrazdelenia])
)
go

Create table [Armia]
(
    [nazvanie_armii]      Char(10) NOT NULL,
    [Nomer_podrazdelenia] Char(10) NOT NULL,
    Constraint [pk_armia] Primary Key ([nazvanie_armii], [Nomer_podrazdelenia])
)
go

Create table [Soorugenia]
(
    [Nomer_coorugenia]        Char(10) NOT NULL,
    [Nomer_disloc_obedinenia] Char(10) NOT NULL,
    [Nomer_podrazdelenia]     Char(10) NOT NULL,
    Constraint [pk_Soorugenia] Primary Key ([Nomer_coorugenia], [Nomer_disloc_obedinenia], [Nomer_podrazdelenia])
)
go

Create table [Tehnika]
(
    [Tip_tehniki]         Char(10) NOT NULL,
    [Kol_vo_tehniki]      Integer  NOT NULL,
    [Nomer_podrazdelenia] Char(10) NOT NULL,
    Constraint [pk_Tehnika] Primary Key ([Tip_tehniki], [Kol_vo_tehniki], [Nomer_podrazdelenia])
)
go

Create table [Podrazdelenie]
(
    [Nomer_podrazdelenia] Char(10) NOT NULL,
    Constraint [pk_Podrazdelenie] Primary Key ([Nomer_podrazdelenia])
)
go

Create table [Voorugenie]
(
    [Tip_voorug]          Char(10) NOT NULL,
    [Kol_vo_voorug]       Integer  NOT NULL,
    [Nomer_podrazdelenia] Char(10) NOT NULL,
    Constraint [pk_Voorugenie] Primary Key ([Tip_voorug], [Kol_vo_voorug], [Nomer_podrazdelenia])
)
go