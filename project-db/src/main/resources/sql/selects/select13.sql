SELECT
       nazvanie_armii,
       COUNT(Nomer_podrazdelenia) AS col
FROM chast
GROUP BY nazvanie_armii
    HAVING COUNT(nazvanie_armii)>=ALL(SELECT COUNT(nazvanie_armii)
        FROM Chast
        GROUP BY nazvanie_armii)