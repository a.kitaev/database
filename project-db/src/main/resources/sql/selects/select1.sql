SELECT
       chast.Nazvanie_chasti,
       chast.Nomer_podrazdelenia,
       chast.Mesto,
       chast.tip_i_nomer_obedinenia,
       chast.nazvanie_armii,
       sostav.FIO,
       sostav.Zvanie,
       sostav.Kod_slugashego
FROM chast, sostav
WHERE chast.nazvanie_armii=:par1 AND sostav.Nomer_podrazdelenia=chast.Nomer_podrazdelenia