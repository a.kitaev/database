SELECT
       chast.Nazvanie_chasti,
       chast.Nomer_podrazdelenia,
       chast.Mesto,
       chast.tip_i_nomer_obedinenia,
       chast.nazvanie_armii,
       sostav.FIO,
       sostav.Zvanie,
       sostav.Kod_slugashego
FROM chast, sostav
WHERE chast.Nomer_podrazdelenia=sostav.Nomer_podrazdelenia AND sostav.Tip_sostava=:par1